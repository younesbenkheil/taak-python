import requests, json,re,yaml

x = requests.get('https://api.domainsdb.info/v1/domains/search?domain=syntra.be').json()

domains = x["domains"]

createddate_full = domains[0]["create_date"]
provider_full = domains[0]["NS"][0]
ip = domains[0]["A"][0]




jaar = re.match("([0-9]+)-([0-9]+)-([0-9]+)",createddate_full).group(1)
maand = re.match("([0-9]+)-([0-9]+)-([0-9]+)",createddate_full).group(2)
dag = re.match("([0-9]+)-([0-9]+)-([0-9]+)",createddate_full).group(3)

provider = re.search("\.([^\.]+)\.",provider_full).group(1)

country = domains[0]["country"]




output = [{'created': {'dag':dag,'jaar':jaar,'maand':maand}},
         {'ip': ip},{'land': country},{'provider':provider}]

print(yaml.dump(output))